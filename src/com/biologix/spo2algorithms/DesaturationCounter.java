/*
 * Copyright (c) 2020 - Biologix Sistemas Ltda.
 * This software is licensed under the MIT License. See README.md for more info.
 */

package com.biologix.spo2algorithms;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * The DesaturationCounter class contains the desaturation counter algorithm, which can be accessed from other classes
 * by invoking the static method countDesaturations. It also contains a simple command-line tool so it can process
 * a sample input file directly by running the class main method.
 */
public class DesaturationCounter {

    private DesaturationCounter() {}

    /**
     * Peak detection utility class.
     */
    private static class PeakDetector {

        public static class Peak {

            public Type type;
            public float value;
            public int index;

        }

        public enum Type {
            HI_PEAK,
            LO_PEAK
        }

        private enum Direction {
            UNKNOWN,
            UP,
            DOWN
        }

        private final Peak mPeak1, mPeak2;
        private Peak mCurPeak;

        private float mPrevValue;
        private int mIndex;
        private Direction mDirection = Direction.UNKNOWN;

        private final float mDebounceUp, mDebounceDown;

        public PeakDetector(float debounceUp, float debounceDown) {

            mDebounceUp = debounceUp;
            mDebounceDown = debounceDown;

            mPeak1 = new Peak();
            mPeak2 = new Peak();

            mCurPeak = mPeak1;

        }

        private void switchCurPeak() {
            mCurPeak = (mCurPeak == mPeak1) ? mPeak2 : mPeak1;
        }

        /* Note: returned Peak objects are recycled between addSample invocations */

        public Peak addSample(float value) {

            Peak returnPeak = null;

            if (mIndex > 0) {

                switch (mDirection) {

                    case UNKNOWN:

                        if (value > mPrevValue) {

                            mCurPeak.type = Type.HI_PEAK;
                            mCurPeak.index = mIndex;
                            mCurPeak.value = value;

                            mDirection = Direction.UP;

                        } else if (value < mPrevValue) {

                            mCurPeak.type = Type.LO_PEAK;
                            mCurPeak.index = mIndex;
                            mCurPeak.value = value;

                            mDirection = Direction.DOWN;

                        }

                        break;

                    case UP:

                        if (value > mCurPeak.value) {

                            mCurPeak.index = mIndex;
                            mCurPeak.value = value;

                        } else if (value < mCurPeak.value - mDebounceUp) {

                            returnPeak = mCurPeak;
                            switchCurPeak();

                            mCurPeak.type = Type.LO_PEAK;
                            mCurPeak.index = mIndex;
                            mCurPeak.value = value;

                            mDirection = Direction.DOWN;

                        }

                        break;

                    case DOWN:

                        if (value < mCurPeak.value) {

                            mCurPeak.index = mIndex;
                            mCurPeak.value = value;

                        } else if (value > mCurPeak.value + mDebounceDown) {

                            returnPeak = mCurPeak;
                            switchCurPeak();

                            mCurPeak.type = Type.HI_PEAK;
                            mCurPeak.index = mIndex;
                            mCurPeak.value = value;

                            mDirection = Direction.UP;

                        }

                        break;

                }

            }

            mIndex++;
            mPrevValue = value;
            return returnPeak;

        }

    }

    /**
     * Determines the total time with valid (non-NaN) SpO2 data.
     * @param validDurationSkipSecs initial duration to be skipped (in seconds), to wait for the signal to stabilize
     * @param spO2 SpO2 data array (sample rate = 1 Hz)
     * @param totalDuration total duration (in seconds), which must be at most the SpO2 data length
     * @return signal valid duration (in seconds)
     */
    public static int resolveValidDuration(int validDurationSkipSecs, float[] spO2, int totalDuration) {

        if (totalDuration > spO2.length) {
            throw new IndexOutOfBoundsException();
        }

        int validDuration = 0;

        for (int i = validDurationSkipSecs; i < totalDuration; i++) {

            if (!Float.isNaN(spO2[i])) {
                validDuration++;
            }

        }

        return validDuration;

    }

    private static final float SPO2_INIT = 96;

    /**
     * Desaturation-counting algorithm.
     * @param desatThreshold minimum SpO2 drop (in %) do count a desaturation
     * @param desatDebounceDown minimum SpO2 rise (in %), after a local minimum, to confirm the desaturation. This
     *                          increases the robustness of the peak detection algorithm by eliminating small peaks
     *                          which can interfere with the counting. To disable this, it can be 0.
     * @param validDurationSkipSecs initial duration to be skipped (in seconds), to wait for the signal to stabilize
     * @param spO2 SpO2 data array (sample rate = 1 Hz)
     * @param totalDuration total duration (in seconds), which must be at most the SpO2 data length
     * @return a list containing the times (in seconds after the data start) when a desaturation was found.
     */
    public static List<Integer> countDesaturations(int desatThreshold, int desatDebounceDown, int validDurationSkipSecs,
                                                 float[] spO2, int totalDuration) {

        if (totalDuration > spO2.length) {
            throw new IndexOutOfBoundsException();
        }

        ArrayList<Integer> desatTimes = new ArrayList<>();
        PeakDetector peakDetector = new PeakDetector(0, desatDebounceDown);

        float desatStartLevel = spO2[0];

        if (Float.isNaN(desatStartLevel)) {
            desatStartLevel = SPO2_INIT;
        }

        int desatStartTime = 0;

        float curSpO2 = SPO2_INIT;

        for (int i = 0; i < totalDuration; i++) {

            float newSpO2 = spO2[i];

            if (!Float.isNaN(newSpO2)) {
                curSpO2 = newSpO2;
            }

            PeakDetector.Peak p = peakDetector.addSample(curSpO2);

            if (p == null) {
                continue;
            }

            switch (p.type) {

                case LO_PEAK: {

                    int desatEndTime = p.index;

                    if (desatEndTime >= validDurationSkipSecs) {

                        float desatEndLevel = p.value;
                        float desatAmount   = desatStartLevel - desatEndLevel;
                        int    desatDuration = desatEndTime    - desatStartTime;

                        if (desatAmount > desatThreshold - 0.001 && desatDuration/desatAmount < 20) {

                            int invalidDuration = 0;
                            for (int j = desatStartTime; j < desatEndTime; j++) {
                                if (Float.isNaN(spO2[j])) {
                                    invalidDuration++;
                                }
                            }

                            if ((float) invalidDuration / desatDuration < 0.25) {
                                desatTimes.add(desatEndTime);
                            }

                        }

                    }

                    break;

                }

                case HI_PEAK: {

                    desatStartLevel = p.value;
                    desatStartTime = p.index;

                    break;

                }

            }

        }

        return desatTimes;

    }

    /**
     * Utility function to read a file to a string.
     * @param file input file
     * @param charset file charset
     * @return file contents
     * @throws IOException if a read error occurs
     */
    private static String readFile(File file, Charset charset) throws IOException {

        try (FileInputStream is = new FileInputStream(file);
             ByteArrayOutputStream os = new ByteArrayOutputStream()) {

            byte[] buf = new byte[16384];

            while (true) {

                int n = is.read(buf);

                if (n == -1) {
                    break;
                }

                os.write(buf, 0, n);

            }

            return new String(os.toByteArray(), charset);

        }

    }

    private static final int DEFAULT_DESAT_THRESHOLD = 3;
    private static final int DEFAULT_DESAT_DEBOUNCE = 2;

    /**
     * Command-line utility entry point.
     * @param args arguments
     * @throws Exception if something happens
     */
    public static void main(String[] args) throws Exception {

        System.out.println(
                "----------------------------------------------\n" +
                " DesaturationCounter\n"  +
                " Copyright (c) 2020 - Biologix Sistemas Ltda.\n" +
                "----------------------------------------------");

        if (args.length == 0) {

            System.out.println(
                    "Usage: DesaturationCounter (input_file) [desat_ths] [debounce] [output_file]\n" +
                    "  input_file (required): the input SpO2 file, sampled at 1Hz\n" +
                    "  desat_ths (optional, default " + DEFAULT_DESAT_THRESHOLD + "): downward SpO2 threshold (in %) to count a desaturation\n" +
                    "  debounce (optional, default " + DEFAULT_DESAT_DEBOUNCE + "): upward SpO2 threshold (in %) to count a desaturation\n" +
                    "  output_file (optional): file to receive the times (s) when desaturations occurred\n");

            return;

        }

        File inputFile = new File(args[0]);
        int desatThreshold = (args.length < 2) ? DEFAULT_DESAT_THRESHOLD : Integer.parseInt(args[1]);
        int desatDebounce = (args.length < 3) ? DEFAULT_DESAT_DEBOUNCE : Integer.parseInt(args[2]);
        File outputFile = (args.length < 4) ? null : new File(args[3]);

        System.out.println("\nParameters used:");
        System.out.printf("  SpO2 desaturation threshold: %d%%\n", desatThreshold);
        System.out.printf("  SpO2 desaturation debounce: %d%%\n", desatDebounce);

        System.out.println("\nReading input file '" + inputFile.getAbsolutePath() + "'...");

        String[] spO2S = readFile(inputFile, StandardCharsets.UTF_8).split("\\n");

        float[] spO2 = new float[spO2S.length];

        for (int i = 0; i < spO2S.length; i++) {
            try {
                spO2[i] = Float.parseFloat(spO2S[i]);
            } catch (NumberFormatException e) {
                spO2[i] = Float.NaN;
            }
        }

        int totalDurationSecs = spO2.length;

        System.out.printf("Input total duration: %02d:%02d:%02d (%d s)\n", totalDurationSecs / 3600,
                (totalDurationSecs / 60) % 60, totalDurationSecs % 60, totalDurationSecs);

        System.out.println("\nProcessing input...");

        int validDurationSecs = resolveValidDuration(0, spO2, totalDurationSecs);

        List<Integer> desatTimes = countDesaturations(desatThreshold, desatDebounce, 0, spO2, totalDurationSecs);

        System.out.println("\nResults:");

        System.out.printf("  Input valid duration: %02d:%02d:%02d (%d s)\n", validDurationSecs / 3600,
                (validDurationSecs / 60) % 60, validDurationSecs % 60, validDurationSecs);

        System.out.printf("  Number of desaturations: %d\n", desatTimes.size());

        System.out.printf("  Oxygen Desaturation Index (ODI): %.1f /hour\n",
                (validDurationSecs > 0) ? (3600.0f * desatTimes.size() / validDurationSecs) : 0.0f);

        if (outputFile != null) {

            System.out.println("\nWriting output file '" + outputFile.getAbsolutePath() + "'...");

            StringBuilder sb = new StringBuilder();

            for (int desatTime : desatTimes) {
                sb.append(desatTime).append("\n");
            }

            try (FileOutputStream os = new FileOutputStream(outputFile)) {
                os.write(sb.toString().getBytes(StandardCharsets.UTF_8));
            }

        }

        System.out.println("\nDone.");

    }

}
