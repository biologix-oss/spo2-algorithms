----------------------------------------------
 DesaturationCounter
 Copyright (c) 2020 - Biologix Sistemas Ltda.
----------------------------------------------

Parameters used:
  SpO2 desaturation threshold: 3%
  SpO2 desaturation debounce: 2%

Reading input file '/home/filipe/Projects/idea/spo2algorithms/samples/DesaturationCounter/patient1-input.txt'...
Input total duration: 08:13:50 (29630 s)

Processing input...

Results:
  Input valid duration: 08:13:27 (29607 s)
  Number of desaturations: 154
  Oxygen Desaturation Index (ODI): 18,7 /hour

Writing output file '/home/filipe/Projects/idea/spo2algorithms/samples/DesaturationCounter/patient1-output.txt'...

Done.
