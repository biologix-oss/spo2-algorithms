# SpO2 algorithms

This project provides algorithms and utilities being used in Biologix products.
These algorithms provide processing and information-extracting methods for
SpO2 measurements.

## DesaturationCounter

This algorithm counts SpO2 desaturations (drops in the SpO2 curve) from an
input SpO2 data series.

JDK 8+ is required to compile/run the file.

To compile, open a command-line tool and type:

```
javac -d target src/com/biologix/spo2algorithms/DesaturationCounter.java
```

Now, you can run the algorithm by typing:

```
java -cp target com.biologix.spo2algorithms.DesaturationCounter
```


This will show the usage options.

To process a sample input file, type:

```
java -cp target com.biologix.spo2algorithms.DesaturationCounter samples/DesaturationCounter/patient0-input.txt
```

## License

All code available here is licensed under *the MIT License* (see LICENSE).